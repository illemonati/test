

// Flag starter kit

/*
 * YOUR NAME HERE
 * YOUR PARTNER'S NAME HERE
 * OTHER PARTNER'S NAME (if group of three)
 */
<applet code="Flag.class" CodeBase="" width=300 height=400></applet>
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JApplet;


class Flag extends JApplet {
	private final int STRIPES = 13;

	// SCALE FACTORS (A through L)
	//
	// Note: Constants in Java should always be ALL_CAPS, even
	// if we are using single letters to represent them
    //
    // NOTE 2: Do not delete or change the names of any of the
    // variables given here

	// Set the constants to exactly what is specified in the documentation
	private final double A = 0;  // Hoist (width) of flag
	private final double B = 0;  // Fly (length) of flag
	private final double C = 0;  // Hoist of Union
	private final double D = 0;  // Fly of Union
	private final double E = 0;  // See flag specification
	private final double F = 0;  // See flag specification
	private final double G = 0;  // See flag specification
	private final double H = 0;  // See flag specification
	private final double K = 0;  // Diameter of star
	private final double L = 0;  // Width of stripe

    // You will need to set values for these in paint()
	private double flag_width;   // width of flag in pixels
	private double flag_height;  // height of flag in pixels
	private double stripe_height; // height of an individual stripe in pixels

    // init() will automatically be called when an applet is run
	public void init() {
		// Choice of width = 1.9 * height to start off
		// 760 : 400 is ratio of FLY : HOIST
		setSize(760, 400);
		repaint();
	}

    // paint() will be called every time a resizing of an applet occurs
	public void paint(Graphics g) {
	}

	private void drawBackground(Graphics g) {
	}

	public void drawStripes(Graphics g) {
	}

	public void drawField(Graphics g) {
	}

	public void drawStars(Graphics g) {
	}
}
