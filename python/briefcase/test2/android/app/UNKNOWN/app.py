
import toga

# class MyApplication:
#     def onStart(self):
#         print("Application starting up")
#         main().main_loop()




def button_handler(widget):
    print("hello")


def build(app):
    box = toga.Box()
    button = toga.Button('Hello world', on_press=button_handler)
    button.style.padding = 50
    button.style.flex = 1
    box.add(button)
    return box


def main():
    return toga.App('Test 1', 'tech.tioft.test1', startup=build)


app = MyApplication()
activity = PythonActivity.setListener(main())