#!/usr/bin/env python3

import sys

class sqrt_py:
    def __init__(self):
        self.main()

    def main(self):
        try:
            self.og = float(sys.argv[1])
            answer = self.sqrt(self.og)
            print(answer)
        except Exception as e:
            print("Something went wrong, try putting in a argument.")

    def sqrt(self,d):
        return d*d


if __name__ == '__main__':
    sqrt_py()
