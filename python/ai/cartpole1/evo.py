import gym
import os
import neat
import pickle

def eval_genome(genome, config):
    net = neat.nn.FeedForwardNetwork.create(genome, config)
    total_reward = 0
    env = gym.make('CartPole-v0')
    state = env.reset()
    steps = 10000
    steps_taken = 0
    for timestep in range(steps):
        action = net.activate(state)[0]
        action = round(action)
        next_state, reward, done, info = env.step(action)
        total_reward += reward
        state = next_state
        steps_taken += 1
        if done:
            break
    return total_reward


def train(config):
    p = neat.Population(config)
    p.add_reporter(neat.StdOutReporter(True))
    p.add_reporter(neat.StatisticsReporter())
    p.add_reporter(neat.Checkpointer(10))
    pe = neat.ParallelEvaluator(16, eval_genome)
    best = p.run(pe.evaluate, 300)
    return best


def test(best, config):
    best_net = neat.nn.FeedForwardNetwork.create(best, config)
    total_reward = 0
    env = gym.make('CartPole-v0')
    state = env.reset()
    done = False
    while not done:
        env.render()
        action = best_net.activate(state)[0]
        action = round(action)
        next_state, reward, done, info = env.step(action)
        total_reward += reward
        state = next_state


if __name__ == "__main__":
    config_file = "config-feedfoward"
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                        neat.DefaultSpeciesSet, neat.DefaultStagnation,
                        config_file)
    best = train(config)
    best_file = open('best.genome', 'wb')
    pickle.dump(best, best_file)
    print(best)
    
    best_file = open('best.genome', 'rb')
    best = pickle.load(best_file)
    test(best, config)
