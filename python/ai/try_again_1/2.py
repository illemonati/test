import gym

from IPython.display import clear_output
from time import sleep
import numpy as np


def print_frames(frames):
    for i, frame in enumerate(frames):
        clear_output(wait=True)
        print(frame['frame'].getvalue())
        print(f"Timestep: {i + 1}")
        print(f"State: {frame['state']}")
        print(f"Action: {frame['action']}")
        print(f"Reward: {frame['reward']}")
        print(f"t_reward: {frame['t_reward']}")
        sleep(.1)



F = open("2.save","wb")
env = gym.make("Taxi-v2").env
state = env.encode(3, 1, 2, 0) # (taxi row, taxi column, passenger index, destination index)
env.s = state  # set environment to illustration's state
q_table = np.zeros([env.observation_space.n, env.action_space.n])

epochs = 0
penalties, reward = 0, 0

frames = [] # for animation

done = False
EPISODES = 2000000

import random
from IPython.display import clear_output

# Hyperparameters
alpha = 0.1
gamma = 0.6
epsilon = 0.1

# For plotting metrics
all_epochs = []
all_penalties = []


for i in range(0, EPISODES):
    state = env.reset()

    epochs, penalties, reward, = 0, 0, 0
    done = False

    while not done:
        if random.uniform(0, 1) < epsilon:
            action = env.action_space.sample() # Explore action space
        else:
            action = np.argmax(q_table[state]) # Exploit learned values

        next_state, reward, done, info = env.step(action)

        old_value = q_table[state, action]
        next_max = np.max(q_table[next_state])

        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        q_table[state, action] = new_value

        if reward == -10:
            penalties += 1

        state = next_state
        epochs += 1

    if i % 1000 == 0:
        clear_output(wait=True)
        print(f"Episode: {i}")
        np.save(F, q_table)

print("Training finished.\n")


print("Timesteps taken: {}".format(epochs))
print("Penalties incurred: {}".format(penalties))

print("Action Space {}".format(env.action_space))
print("State Space {}".format(env.observation_space))

env.reset()

done = False
t_reward = 0
while not done:
    action = np.argmax(q_table[state])
    state, reward, done, info = env.step(action)
    t_reward += reward

    frames.append({
            'frame': env.render(mode='ansi'),
            'state': state,
            'action': action,
            'reward': reward,
            't_reward' : t_reward,
            }
        )


print_frames(frames)
print(q_table)
