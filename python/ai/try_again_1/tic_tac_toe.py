import gym

from IPython.display import clear_output
from time import sleep
import numpy as np
# rom gym_tictactoe.env import TicTacToeEnv
from gym_tictactoe.env import TicTacToeEnv


def print_frames(frames):
    for i, frame in enumerate(frames):
        clear_output(wait=True)
        print(frame['frame'].getvalue())
        print(f"Timestep: {i + 1}")
        print(f"State: {frame['state']}")
        print(f"Action: {frame['action']}")
        print(f"Reward: {frame['reward']}")
        sleep(.1)



env = TicTacToeEnv()

# env.s = state  # set environment to illustration's state
state = env.s
print(state)
q_table = np.zeros([env.observation_space.n, env.action_space.n])

epochs = 0
penalties, reward = 0, 0

frames = [] # for animation

done = False

# while not done:
#     action = env.action_space.sample()
#     state, reward, done, info = env.step(action)
#
#     if reward == -10:
#         penalties += 1
#
#     # Put each rendered frame into dict for animation
#     frames.append({
#         'frame': env.render(mode='ansi'),
#         'state': state,
#         'action': action,
#         'reward': reward
#         }
#     )
#
#     epochs += 1
#
# print_frames(frames)

import random
from IPython.display import clear_output

# Hyperparameters
alpha = 0.1
gamma = 0.6
epsilon = 0.1

# For plotting metrics
all_epochs = []
all_penalties = []

for i in range(1, 100001):
    state = env.reset()

    epochs, penalties, reward, = 0, 0, 0
    done = False

    while not done:
        if random.uniform(0, 1) < epsilon:
            action = env.action_space.sample() # Explore action space
        else:
            action = np.argmax(q_table[state]) # Exploit learned values

        next_state, reward, done, info = env.step(action)

        old_value = q_table[state, action]
        next_max = np.max(q_table[next_state])

        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        q_table[state, action] = new_value

        if reward == -10:
            penalties += 1

        state = next_state
        epochs += 1

    if i % 100 == 0:
        clear_output(wait=True)
        print(f"Episode: {i}")

print("Training finished.\n")


print("Timesteps taken: {}".format(epochs))
print("Penalties incurred: {}".format(penalties))
# print("Action Space {}".format(env.action_space))
# print("State Space {}".format(env.observation_space))


#print the optimal solution
env.s = env.encode(3, 1, 2, 0) # (taxi row, taxi column, passenger index, destination index)


done = False
while not done:
    action = np.argmax(q_table[state])
    state, reward, done, info = env.step(action)
    frames.append({
            'frame': env.render(mode='ansi'),
            'state': state,
            'action': action,
            'reward': reward
            }
        )


print_frames(frames)
