import gym

from IPython.display import clear_output
from time import sleep
import numpy as np
import random
import gym
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

def print_frames(frames):
    for i, frame in enumerate(frames):
        clear_output(wait=True)
        print(frame['frame'].getvalue())
        print(f"Timestep: {i + 1}")
        print(f"State: {frame['state']}")
        print(f"Action: {frame['action']}")
        print(f"Reward: {frame['reward']}")
        sleep(.5)



env = gym.make("Taxi-v2").env
state = env.encode(3, 1, 2, 0) # (taxi row, taxi column, passenger index, destination index)
env.s = state  # set environment to illustration's state
q_table = np.zeros([env.observation_space.n, env.action_space.n])

epochs = 0
penalties, reward = 0, 0

frames = [] # for animation

import random
from IPython.display import clear_output
EPISODES = 1000

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(24, input_dim=self.state_size, activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = (reward + self.gamma *
                          np.amax(self.model.predict(next_state)[0]))
            target_f = self.model.predict(state)
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)




state_size = env.observation_space.n
action_size = env.action_space.n
agent = DQNAgent(state_size, action_size)
done = False
batch_size = 32


for i in range(1, 20000):
    state = env.reset()

    done = False
    while not done:


        action = agent.act(state)
        next_state, reward, done, _ = env.step(action)
        frames.append({
                'frame': env.render(mode='ansi'),
                'state': next_state,
                'action': action,
                'reward': reward
                }
            )
        reward = reward if not done else -10
        next_state = np.reshape(next_state, [1, state_size])
        agent.remember(state, action, reward, next_state, done)
        state = next_state
        if done:
            print("episode: {}/{}, score: {}, e: {:.2}"
                  .format(e, EPISODES, time, agent.epsilon))
            break
        if len(agent.memory) > batch_size:
            agent.replay(batch_size)

print("Training finished.\n")


print("Timesteps taken: {}".format(epochs))
print("Penalties incurred: {}".format(penalties))
# print("Action Space {}".format(env.action_space))
# print("State Space {}".format(env.observation_space))


print_frames(frames)
