import gym
import numpy as np

STEPS_TO_RUN = 1000
EPISODES_TO_RUN = 20

""" setup """
env = gym.make("Taxi-v2")
env.reset()
highscore = 0
""""""

#
# for i_episode in range(EPISODES_TO_RUN):
#     observation = env.reset() # reset for each new trial
#     points = 0
#
#     for t in range(STEPS_TO_RUN): # run for 100 timesteps or until done, whichever is first
#         env.render()
#         action = env.action_space.sample() # select a random action (see https://github.com/openai/gym/wiki/CartPole-v0)
#         # action = 1 if observation[2] > 0 else 0
#         observation, reward, done, info = env.step(action)
#         points += reward
#         if done:
#             print("Episode finished after {} timesteps; {} points".format(t+1, points))
#             if points > highscore:
#                 highscore = points
#             break





"""Training the agent"""

import random
from IPython.display import clear_output

q_table = np.zeros([env.observation_space.n,env.action_space.n])

# Hyperparameters
alpha = 0.1
gamma = 0.6
epsilon = 0.1

# For plotting metrics
all_epochs = []
all_penalties = []

for i in range(1, 50000):
    state = env.reset()

    epochs, penalties, reward, = 0, 0, 0
    done = False

    while not done:
        if random.uniform(0, 1) < epsilon:
            action = env.action_space.sample() # Explore action space
        else:
            action = np.argmax(q_table[state]) # Exploit learned values

        next_state, reward, done, info = env.step(action)

        old_value = q_table[state, action]
        next_max = np.max(q_table[next_state])

        new_value = (1 - alpha) * old_value + alpha * (reward + gamma * next_max)
        q_table[state, action] = new_value

        if reward == -10:
            penalties += 1

        state = next_state
        epochs += 1

    if i % 100 == 0:
        clear_output(wait=True)
        print(f"Episode: {i}")

print("Training finished.\n")

total_epochs, total_penalties = 0, 0
episodes = 100

for _ in range(episodes):
    state = env.reset()
    epochs, penalties, reward = 0, 0, 0

    done = False

    while not done:
        action = np.argmax(q_table[state])
        state, reward, done, info = env.step(action)

        if reward == -10:
            penalties += 1

        epochs += 1

    total_penalties += penalties
    total_epochs += epochs

print(f"Results after {episodes} episodes:")
print(f"Average timesteps per episode: {total_epochs / episodes}")
print(f"Average penalties per episode: {total_penalties / episodes}")


F = open("2.save","wb")
np.save(F, q_table)
