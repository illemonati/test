import gym
import numpy as np

STEPS_TO_RUN = 1000
EPISODES_TO_RUN = 20

""" setup """
env = gym.make("CartPole-v0")
env.reset()
highscore = 0
""""""


for i_episode in range(EPISODES_TO_RUN):
    observation = env.reset() # reset for each new trial
    points = 0
    action = np.random.rand(4) * 2 - 1

    for t in range(STEPS_TO_RUN): # run for 100 timesteps or until done, whichever is first
        env.render()
        # action = env.action_space.sample() # select a random action (see https://github.com/openai/gym/wiki/CartPole-v0)

        # action = 1 if observation[2] > 0 else 0
        observation, reward, done, info = env.step(action)
        points += reward
        if done:
            print("Episode finished after {} timesteps; {} points".format(t+1, points))
            if points > highscore:
                highscore = points
            break

print(highscore)
