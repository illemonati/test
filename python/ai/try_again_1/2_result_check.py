
from IPython.display import clear_output
import numpy as np
import gym
from time import sleep

def print_frames(frames):
    for i, frame in enumerate(frames):
        clear_output(wait=True)
        print(frame['frame'].getvalue())
        print(f"Timestep: {i + 1}")
        print(f"State: {frame['state']}")
        print(f"Action: {frame['action']}")
        print(f"Reward: {frame['reward']}")
        print(f"t-Reward: {frame['t_reward']}")
        sleep(.5)


# env.s = env.encode(3, 1, 2, 0) # (taxi row, taxi column, passenger index, destination index)

savefile = open("2.save", "rb")
q_table = np.load(savefile)

env = gym.make("Taxi-v2").env
env.reset()
state = env.s
frames = []

t_reward = 0
done = False
while not done:
    action = np.argmax(q_table[state])
    state, reward, done, info = env.step(action)
    t_reward += reward
    frames.append({
            'frame': env.render(mode='ansi'),
            'state': state,
            'action': action,
            'reward': reward,
            't_reward': t_reward,
            }
        )


print_frames(frames)
print(q_table)
