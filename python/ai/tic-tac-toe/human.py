import tictactoe
import dqn
import numpy as np

class HumanPlayer(tictactoe.Player):
    def __init__(self, player_number):
        super().__init__(player_number)
        self.play_number
    
    def get_action(self, state):
        print(state)
        return int(input("put in action\n"))

    def feedback(self, state, new_state, action, over, winner):
        pass


if __name__ == "__main__":
    player1 = dqn.DQN(1)
    player2 = HumanPlayer(-1)
    player1.load_model('a.h5')
    # print(player1.model_a.predict()
    player1.epsilon = 0
    game = tictactoe.TicTacToe(player1, player2)
    game.run()