import tictactoe
import random_player
import numpy as np
import random
from tensorflow import keras
import pickle
from collections import deque



class TableModel:
    def __init__(self):
        self.table = {}

    def fit(self, x, y, epochs=1, verbose=0, batch_size=1):
        self.table[str(x)] = y
        # print(self.table)
    
    def predict(self, state):
        if str(state) not in self.table:
            # print(1)
            return np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0]])
        else:
            print(self.table[str(state)])
            return self.table[str(state)]

    def summary(self):
        return "This is just a dictionary"

    def save(self, name):
        pickle.dump(self.table, open(name, 'wb'))

    @staticmethod
    def load(name):
        tm = TableModel()
        tm.table = pickle.load(open(name, 'rb'))
        return tm

        



class DQN(tictactoe.Player):
    def __init__(self, player_number):
        super().__init__(player_number)
        self.player_number  = player_number
        self.epsilon        = 1.0
        self.epsilon_decay  = 0.98
        self.epsilon_min    = 0.01
        self.alpha          = 0.95
        self.gamma          = 0.80
        self.memory         = deque(maxlen=2000)
        self.model_a        = self.make_model()
        self.batch_size     = 32
        # self.model_a        = TableModel()

    def make_model(self):
        inputs  = keras.layers.Input(shape=(3, 3))
        flatten = keras.layers.Flatten(input_shape=(3, 3))(inputs)
        dense1  = keras.layers.Dense(32)(flatten)
        dense2  = keras.layers.Dense(32)(dense1)
        dense3  = keras.layers.Dense(64)(dense2)
        outputs = keras.layers.Dense(9)(dense3)
        
        model   = keras.Model(inputs=inputs, outputs=outputs)
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=self.alpha), loss=keras.losses.CategoricalCrossentropy(), metrics=['mse', 'mae'])
        return model

    
    def get_action(self, state):
        if random.random() < self.epsilon:
            state = np.ndarray.flatten(state)
            choices = []
            for i in range(len(state)):
                if state[i] == 0:
                    choices.append(i)
            return random.choice(choices)
        state = np.array([state])
        # print(np.nan_to_num(self.model_a.predict(state)))
        # print(np.argmax(np.nan_to_num(self.model_a.predict(state))))
        # maxs =  np.argwhere(np.nan_to_num(self.model_a.predict(state)) == np.amax(np.nan_to_num(self.model_a.predict(state)))).flatten()
        # # print(self.model_a.predict(state))
        # state = np.ndarray.flatten(state)
        # choices = []
        # for i in range(len(state)):
        #     if state[i] == 0:
        #         choices.append(i)
        # print(maxs)
        # for m in maxs:
        #     if m in choices:
        #         return m
        # return random.choice(choices)
        return np.argmax(np.nan_to_num(self.model_a.predict(state)))
    
    def feedback(self, state, new_state, action, over, winner):
        state = np.array([state])
        new_state = np.array([new_state])
        
        old = self.model_a.predict(state)

        new = np.copy(old)
        reward = 0.0
        # if new_state.all() == state.all():
        #     reward = -1

        if over:
            if winner == self.player_number:
                reward = 1
            elif winner == 0-self.player_number:
                reward = 0
            elif winner == 0:
                reward = 0.5

        self.memory.append((state, new_state, action, reward, over))
        if over:
            self.replay()
        #     new[0][action] = (reward)
        # else:
        #     new[0][action] += (reward + self.gamma * np.nan_to_num(np.max(self.model_a.predict(new_state)[0])))
        # self.model_a.fit(x=state, y=new, epochs=1, verbose=0, batch_size=1)
        # # print(reward)
        # # print(state, old, action, new)
        # # if reward == 1:
        # #     print('reward: {} | action: {} | state: \n{}, nextstate: \n{}'.format(reward, action, state, new_state))

        # self.epsilon *= self.epsilon_decay
        # if self.epsilon < self.epsilon_min:
        #     self.epsilon = self.epsilon_min

    def replay(self):
        if len(self.memory) < self.batch_size:
            return
        batch = random.sample(self.memory, self.batch_size)
        for state, new_state, action, reward, over in batch:
            target = reward
            if not over:
                target = (reward + self.gamma * np.nan_to_num(np.amax(self.model_a.predict(new_state)[0])))
            qvals = self.model_a.predict(state)
            qvals[0][action] = target
            # print('q', qvals)
            self.model_a.fit(state, qvals, verbose=0, epochs=10)
            # print('p', self.model_a.predict(state))
        self.epsilon *= self.epsilon_decay
        if self.epsilon < self.epsilon_min:
            self.epsilon = self.epsilon_min


    def save_model(self, name):
        self.model_a.save(name)

    def load_model(self, name):
        if isinstance(self.model_a, TableModel):
            self.model_a = TableModel.load(name)
        else:
            self.model_a = keras.models.load_model(name)
            self.model_a.compile(optimizer=keras.optimizers.Adam(learning_rate=self.alpha), loss=keras.losses.CategoricalCrossentropy(), metrics=['mse'])




if __name__ == "__main__":
    player2 = random_player.RandomPlayer(-1)
    player1 = DQN(1)
    print(player1.model_a.summary())
    for i in range(501):
        game = tictactoe.TicTacToe(player1, player2)
        game.run()
        if i % 50 == 0:
            player1.save_model('a.h5')
            print(i, player1.epsilon)
        


    