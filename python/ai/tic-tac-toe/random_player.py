import tictactoe
import random
import numpy as np

class RandomPlayer(tictactoe.Player):
    def __init__(self, player_number):
        super().__init__(player_number)

    def get_action(self, state):
        state = np.ndarray.flatten(state)
        choices = []
        # print(state)
        for i in range(9):
            if state[i] == 0:
                choices.append(i)
        return random.choice(choices)
    
    def feedback(self, state, new_state, action, over, winner):
        pass



if __name__ == "__main__":
    p1 = RandomPlayer(1)
    p2 = RandomPlayer(-1)
    game = tictactoe.TicTacToe(p1, p1)
    game.run()
    game.print_board()

