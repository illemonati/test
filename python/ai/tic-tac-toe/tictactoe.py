import numpy as np

class TicTacToe:
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2
        self.board = self.make_board()
    
    def make_board(self):
        return np.array([[0, 0, 0]]*3)

    def player_win(self, player_number):
        for i in range(3):
            if self.board[0][i] == self.board[1][i] == self.board[2][i] == player_number:
                return True
            if self.board[i][0] == self.board[i][1] == self.board[i][2] == player_number:
                return True
        if self.board[0][0] == self.board[1][1] == self.board[2][2] == player_number:
            return True
        if self.board[0][2] == self.board[1][1] == self.board[2][0] == player_number:
            return True
        return False

    def all_filled(self):
        for n in np.ndarray.flatten(self.board):
            if n == 0:
                return False
        return True

    def change(self, player_number, space_number):
        row = space_number//3
        col = space_number%3
        if self.board[row][col] != 0:
            return
        self.board[row][col] = player_number

    def over(self):
        return self.player_win(1) or self.player_win(-1) or self.all_filled()

    def who_won(self):
        if self.player_win(1):
            return 1
        if self.player_win(-1):
            return -1
        if self.all_filled():
            return 0
        else:
            return None

    def print_board(self):
        print('---------------------------------------')
        for i in self.board:
            for j in i:
                print(' ' if j != -1 else '', j, ' ', end='')
            print()
        print('---------------------------------------')

    def run(self):
        first_turn = True
        won2 = None
        while not self.over():
            state1 = np.copy(self.board)
            action1 = self.player1.get_action(self.board)
            self.change(1, action1)
            new_state1 = np.copy(self.board)
            won1 = self.who_won()
            # print(action)
            big_won = None
            if won1 != None:
                big_won = won1
            elif won2 != None:
                big_won = won2
            # print(action1)
            self.player1.feedback(state1, new_state1, action1, self.over(), big_won)
            if not first_turn:
                self.player2.feedback(state2, new_state2, action2, self.over(), big_won)
            if self.over():
                # print(1)    
                break
            state2 = np.copy(self.board)
            action2 = self.player2.get_action(self.board)
            self.change(-1, action2)
            new_state2 = np.copy(self.board)
            won2 = self.who_won()
            big_won = None
            if won1 != None:
                big_won = won1
            elif won2 != None:
                big_won = won2
            self.player1.feedback(state1, new_state1, action1, self.over(), big_won)
            self.player2.feedback(state2, new_state2, action2, self.over(), big_won)
            first_turn = False


class Player:
    def __init__(self, player_number):
        self.play_number = player_number
    
    def get_action(self, state):
        raise NotImplementedError

    def feedback(self, state, new_state, action, over, winner):
        raise NotImplementedError
