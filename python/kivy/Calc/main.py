from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from os import listdir
from decimal import Decimal


#kv_path = './kv/'
#for kv_file in listdir(kv_path):
#    Builder.load_file(kv_path+kv_file)
Builder.load_file('./kv/buttons.kv')

class Container(GridLayout):
    display = ObjectProperty()

    afterFunc = False
    first = True
    value = 0
    pfunc = ''
    afterCalc = False

    def show_num(self,n):
        text = str(self.display.text)
        if self.afterCalc:
            self.first = True
            self.afterCalc = False
            self.value = 0
            self.pfunc = ''
        if (text == '0') or self.afterFunc:
            if n == ".":
                self.display.text = "0"+str(n)
            else:
                self.display.text = str(n)
            self.afterFunc = False
        else:
            self.display.text = str(text + str(n))

    def addFunc(self):
        if not self.first:
            if self.pfunc == '+':
                self.display.text = str(self.value + Decimal(str(self.display.text)))
            if self.pfunc == '-':
                self.display.text = str(self.value - Decimal(str(self.display.text)))
            if self.pfunc == '*':
                self.display.text = str(self.value * Decimal(str(self.display.text)))
            if self.pfunc == '/':
                self.display.text = str(self.value / Decimal(str(self.display.text)))
            if self.afterCalc:
                self.afterCalc = False
                self.first = True

        self.value = Decimal(str(self.display.text))
        self.pfunc = '+'
        self.afterFunc = True
        self.first = False

    def subFunc(self):
        if not self.first:
            if self.pfunc == '+':
                self.display.text = str(self.value + Decimal(str(self.display.text)))
            if self.pfunc == '-':
                self.display.text = str(self.value - Decimal(str(self.display.text)))
            if self.pfunc == '*':
                self.display.text = str(self.value * Decimal(str(self.display.text)))
            if self.pfunc == '/':
                self.display.text = str(self.value / Decimal(str(self.display.text)))
            if self.afterCalc:
                self.afterCalc = False
                self.first = True

        self.value = Decimal(str(self.display.text))
        self.pfunc = '-'
        self.afterFunc = True
        self.first = False

    def multFunc(self):
        if not self.first:
            if self.pfunc == '+':
                self.display.text = str(self.value + Decimal(str(self.display.text)))
            if self.pfunc == '-':
                self.display.text = str(self.value - Decimal(str(self.display.text)))
            if self.pfunc == '*':
                self.display.text = str(self.value * Decimal(str(self.display.text)))
            if self.pfunc == '/':
                self.display.text = str(self.value / Decimal(str(self.display.text)))
            if self.afterCalc:
                self.afterCalc = False
                self.first = True

        self.value = Decimal(str(self.display.text))
        self.pfunc = '*'
        self.afterFunc = True
        self.first = False

    def divFunc(self):
        if not self.first:
            if self.pfunc == '+':
                self.display.text = str(self.value + Decimal(str(self.display.text)))
            if self.pfunc == '-':
                self.display.text = str(self.value - Decimal(str(self.display.text)))
            if self.pfunc == '*':
                self.display.text = str(self.value * Decimal(str(self.display.text)))
            if self.pfunc == '/':
                self.display.text = str(self.value / Decimal(str(self.display.text)))
            if self.afterCalc:
                self.afterCalc = False
                self.first = True

        self.value = Decimal(str(self.display.text))
        self.pfunc = '/'
        self.afterFunc = True
        self.first = False


    def calcFunc(self):
        if not self.first:
            if self.pfunc == '+':
                self.display.text = str(self.value + Decimal(str(self.display.text)))
            if self.pfunc == '-':
                self.display.text = str(self.value - Decimal(str(self.display.text)))
            if self.pfunc == '*':
                self.display.text = str(self.value * Decimal(str(self.display.text)))
            if self.pfunc == '/':
                self.display.text = str(self.value / Decimal(str(self.display.text)))
            if self.afterCalc:
                self.afterCalc = False
                self.first = True

        self.value = Decimal(str(self.display.text))
        self.pfunc = '='
        self.afterFunc = True
        self.first = False
        self.afterCalc = True

    def acFunc(self):
        self.display.text = "0"
        self.afterFunc = False
        self.first = True
        self.value = 0
        self.pfunc = ''
        self.afterCalc = False



class MainApp(App):

    def build(self):
        self.title= 'Tong Calculator'
        return Container()


class Button1(Button):
    pass

class AddButton(Button):
    pass

class SubButton(Button):
    pass

class CalcButton(Button):
    pass

class MultButton(Button):
    pass

class DivButton(Button):
    pass

class ACButton(Button):
    pass

class Button2(Button):
    pass

class Button3(Button):
    pass

class Button4(Button):
    pass

class Button5(Button):
    pass

class Button6(Button):
    pass

class Button7(Button):
    pass

class Button8(Button):
    pass

class Button9(Button):
    pass

class Button0(Button):
    pass

class DecimalButton(Button):
    pass

if __name__ == '__main__':
    app = MainApp()
    app.run()
