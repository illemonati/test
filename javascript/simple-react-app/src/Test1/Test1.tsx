import React from 'react';

const ImageBlocks: React.FC = () => {
    const [counter, setCounter] = React.useState(1e+36);
    return (
        <div>
            <p>The counter is: {counter}</p>
            <button onClick={() => setCounter(counter + 1)}>Increase counter by 1</button>
            <br/>
            <p>Set counter value as: </p>
            <input type="number" onChange={(event) => setCounter(parseInt(event.target.value))} value={counter}/>
        </div>
    )
};

export default ImageBlocks;
