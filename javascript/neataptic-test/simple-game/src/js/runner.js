class Runner {
    constructor ({neat, games, gameSize, gameUnit, frameRate, maxTurns, lowestScoreAllowed, score, onEndGeneration}) {
        this.neat = neat;
        this.games = [];
        this.gamesFinished = 0;
        this.onEndGeneration = onEndGeneration;

        for (let i = 0; i < games; i++) {
            this.games.push(new Game({
                size: gameSize,
                unit: gameUnit,
                frameRate,
                maxTurns,
                lowestScoreAllowed,
                score,
                onGameOver: () => this.endGeneration()
            }))
        }
    }
}
