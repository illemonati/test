


class TotalDisplay {
    constructor() {
        this.app = TotalDisplay.buildApp();
    }

    static buildApp() {
        const app = document.querySelector("#game-body");
        return app;
    }
}

class Episode {
    constructor ({frameRate, maxTurns, lowestScoreAllowed, score, onGameOver, game}) {
        this.frameRate = frameRate;
        this.maxTurns = maxTurns;
        this.lowestScoreAllowed = lowestScoreAllowed;
        this.score = score;
        this.onGameOver = onGameOver;
        this.game = game;
        this.timestep = 0;
        this.hero = new Hero(game.heroLocation ,game, score);
    }
}

class Game {
    constructor(td) {
        this.app = td.app;
        this.container = Game.buildContainer();
        this.app.appendChild(this.container.view);
        this.init();
    }

    static buildContainer() {
        const container = new PIXI.Application({
            width: GAME_WIDTH, height: GAME_HEIGHT, backgroundColor: 0x1099bb, resolution: 10
        });
        return container
    }

    init() {
        const graphics = new PIXI.Graphics();
        graphics.lineStyle(2, 0xFEEB77, 1);
        graphics.beginFill(0x650A5A);
        graphics.drawRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        graphics.endFill();
        const dgraphics = new PIXI.Graphics();
        dgraphics.lineStyle(2, 0xFF0000, 1);
        dgraphics.beginFill(0xFF0000);
        this.redDot = Game.generateCoord();
        dgraphics.moveTo(this.redDot.x, this.redDot.y);
        dgraphics.lineTo(this.redDot.x+1, this.redDot.y+1);
        console.log(this.redDot);
        dgraphics.endFill();
        this.heroLocation = Game.generateCoord();
        dgraphics.lineStyle(2, 0x32cd32, 1);
        dgraphics.beginFill(0x32cd32);
        dgraphics.moveTo(this.heroLocation.x, this.heroLocation.y);
        dgraphics.lineTo(this.heroLocation.x+1, this.heroLocation.y+1);
        console.log(this.heroLocation);
        dgraphics.endFill();
        this.container.stage.addChild(graphics);
        this.container.stage.addChild(dgraphics);
    }

    static generateCoord() {
        return new Coord(Math.floor(Math.random() * (GAME_WIDTH)), Math.floor(Math.random() * (GAME_HEIGHT)));
    }
}

class Coord {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    isSame(coord) {
        if ((this.x === coord.x) && (this.y === coord.y)) {
            return true;
        }
    }
}
