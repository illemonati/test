class Hero {

    init(location, game, score) {
        this.direction = 'up';
        this.location = location;
        this.game = game;
    }

    move() {
        let canMoveUp = 0;
        let canMoveLeft = 0;
        let canMoveRight = 0;
        let canMoveDown = 0;

        if (this.location.y > 1) canMoveUp = 1;
        if (this.location.x > 1) canMoveLeft = 1;
        if (this.location.y < GAME_WIDTH - 1) canMoveRight = 1;
        if (this.location.x < GAME_HEIGHT - 1) canMoveDown = 1;

        const input = [canMoveUp, canMoveDown, canMoveRight, canMoveLeft, this.location.x, this.location.y]
        const output = this.brain.activate(input).map(output => Math.round(output));

        const preMoveDistance = this.calcDistance();

        if (output[0]) {
            this.location.y += 1;
        } else if (output[1]) {
            this.location.y -= 1;
        } else if (output[2]) {
            this.location.x += 1;
        } else if (output[3]) {
            this.location.x -= 1;
        }
        game
        const postMoveDistance = this.calcDistance();
        const distanceChange = preMoveDistance - postMoveDistance;

        this.score += (distanceChange < 0) ? POINTS_MOVED_TOWARDS_FOOD : POINTS_MOVED_AGAINST_FOOD;
        if (this.location.isSame(this.game.redDot.location)) this.score += POINTS_ATE_FOOD;
    }

    calcDistance() {
        return Math.sqrt(((this.location.x - this.game.redDot.location.x)^2) + ((this.location.y - this.game.redDot.location.y)^2))
    }

}
