package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
)

type Encryptor struct {
	data []byte
	gcm cipher.AEAD
	readIndex int64
}

func (en *Encryptor) Read(p []byte) (n int, e error) {
	if en.readIndex >= int64(len(p)) {
		return
	}
	var data []byte = make([]byte, 0)
	nonce := make([]byte, en.gcm.NonceSize())
	if _, err := io.ReadFull(rand.Reader, nonce); err == nil {
		data = en.gcm.Seal(nonce, nonce, en.data[en.readIndex:], nil)
	}
	copy(p, data)
	n = len(en.data[en.readIndex:])
	en.readIndex += int64(n)
	return
}

func (en *Encryptor) Write(p []byte) (n int, e error) {
	en.data = append(en.data, p...)
	return len(p), nil
}

func NewEncryptor() *Encryptor {
	key := fmt.Sprintf("%32v", KEY)
	c, _ := aes.NewCipher([]byte(key))
	gcm, _ := cipher.NewGCM(c)
	return &Encryptor{
		make([]byte, 0),
		gcm,
		0,
	}
}
