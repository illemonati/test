package main

import (
	"fmt"
	"os"
	"text/template"
)

type Calc struct {
	Multiply, Add, Subtract, Divide string
}

func main() {
	file, err := os.Create("calc.go")
	if err != nil {
		panic(err)
	}

	defer file.Close()
	tpl, err := template.ParseFiles("calcTemplate.go")
	if err != nil {
		panic(err)
	}

	calc := Calc{
		Multiply: generateHardcodings('*', 50),
		Add:      generateHardcodings('+', 50),
		Divide:   generateHardcodings('/', 50),
		Subtract: generateHardcodings('-', 50),
	}
	err = tpl.Execute(file, calc)
	if err != nil {
		panic(err)
	}

}

func generateHardcodings(symbol rune, until int) string {
	result := ""

	for i := 0; i < until+1; i++ {
		for j := 0; j < until+1; j++ {
			var nres string
			if symbol == '+' {
				nres = fmt.Sprint(i + j)
			} else if symbol == '-' {
				nres = fmt.Sprint(i - j)
			} else if symbol == '*' {
				nres = fmt.Sprint(i * j)
			} else if symbol == '/' {
				nres = fmt.Sprint(float64(i) / float64(j))
			}
			result += fmt.Sprintf("if n1 == %d && n2 == %d {\n", i, j)
			result += fmt.Sprintf("            return \"%d %c %d = %s\"\n", i, symbol, j, nres)
			result += fmt.Sprintf("        } else ")
		}
	}

	result = result[:len(result)-6]

	return result
}
