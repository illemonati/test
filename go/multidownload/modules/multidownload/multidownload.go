package multidownload

import (
	"fmt"
	"github.com/levigross/grequests"
	"regexp"
	"strconv"
	"sync"
)

var DEBUG bool = false

func Download(url string, threads int, filename string, debug bool)  {
	DEBUG = debug
	canMultithread, leng := parseHeader(url)
	if len(filename) < 1 {
		r := regexp.MustCompile(".+/(.+)")
		filename = r.FindStringSubmatch(url)[1]
	}
	debugPrint(filename)
	debugPrint(canMultithread)
	debugPrint(leng)
	var each int = leng / threads
	var wg sync.WaitGroup = sync.WaitGroup{}
	for i := 0; i < leng; i+=each {
		wg.Add(1)
		go downloadPart(&wg, url, filename, i, i+each)
	}
	wg.Wait()
}

func downloadPart(wg *sync.WaitGroup, url string, filename string, start, end int) {
	defer wg.Done()
	debugPrint(fmt.Sprintf("Downloading %d to %d", start, end))
	ops := &grequests.RequestOptions{
		Headers: map[string]string{"Ranger": fmt.Sprintf("bytes=%d-%d", start, end)},
	}
	res, _ := grequests.Get(url, ops)
	if err := res.DownloadToFile(fmt.Sprintf("%s.part.%d", filename, start/(end-start))); err != nil {
		fmt.Println(err)
	}
}

func parseHeader(url string) (bool, int) {
	res, _ := grequests.Get(url, nil)
	ar := res.Header.Get("Accept-Ranges")
	leng, _ := strconv.Atoi(res.Header.Get("Content-Length"))
	if string(ar) != "bytes" {
		return false, 0
	}
	return true, leng
}

func debugPrint(a ...interface{}) {
	if DEBUG {
		fmt.Println(a)
	}
}


