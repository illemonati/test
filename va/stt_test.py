import speech_recognition as sr


while True:
    r = sr.Recognizer()
    with sr.Microphone() as source:
      r.adjust_for_ambient_noise(source)  # here
      print("Say something!")
      audio = r.listen(source)

    try:
        # print("You said " + r.recognize_google(audio))    # recognize speech using Google Speech Recognition - ONLINE
        print( r.recognize_sphinx(audio))    # recognize speech using CMUsphinx Speech Recognition - OFFLINE
    except LookupError:                            # speech is unintelligible
        print("Could not understand audio")
    except Exception as e:
        print('Error: {}'.format(str(e)))
