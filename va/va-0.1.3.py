#! /usr/bin/env python3

#0.1.2
#Key words only

############################
# 0.1.3 added duckduckgo   #
############################

import speech_recognition as sr
import pyttsx3
import duckduckgo

class VA:
    def __init__(self,kw):
        self.engine = pyttsx3.init()
        self.r = sr.Recognizer()

        self.audios = []
        self.texts = []
        self.kw = kw.lower().split(" ")
        with sr.Microphone() as source:
            self.r.adjust_for_ambient_noise(source)

    def Main(self):
        while True:

            self.listen()

            self.stt()
            # self.check_for_keyword()

            self.ddg_search()

            self.tts()



    def listen(self):
        try:
            with sr.Microphone() as source:

              print("Say something!")
              self.audios.append(self.r.listen(source, snowboy_configuration=('/root/test/va/snowboy/swig/Python3/', ['God.pmdl'])))
        except Exception as e:
            print('Error: {}'.format(str(e)))

    def stt(self):
        for audio in self.audios:
            try:
                t = str(self.r.recognize_google(audio).lower())
                tl = t.split(' ')
                if len(tl) > 1:
                    t = ' '.join(tl[1:])
                    self.texts.append(t)

                print('Q: {}'.format(t))
                self.audios.remove(audio)
                print()
            except LookupError:
                print("Could not understand audio")
            except Exception as e:
                print('Error: {}'.format(str(e)))

    # def check_for_keyword(self):
    #     kw = self.kw
    #     for i, text in enumerate(self.texts):
    #         try:
    #             ts = text.split(' ')
    #             if ts[:len(kw)] == kw:
    #                 ts = ts[len(kw):]
    #                 self.texts[i] = ' '.join(ts)
    #                 print(text)
    #                 print(1)
    #             else:
    #                 self.texts.remove(text)
    #                 print(0)
    #         except Exception as e:
    #             print('Error: {}'.format(str(e)))



    def ddg_search(self):
        for i, text in enumerate(self.texts):
            try:
                text = self.processSearch(text)
                print(text)
                r = duckduckgo.get_zci(text)
                r_list = r.split(' ')
                for w in r_list:
                    if '(https://' in w:
                        r_list.remove(w)
                r = ' '.join(r_list)
                self.texts[i] = r
            except Exception as e:
                print("Error: {}".format(str(e)))
                self.texts[i] = "Error: {}".format(str(e))

    def tts(self):
        for text in self.texts:
            try:
                self.engine.say(text)
                print(text)
                self.engine.runAndWait()
                self.texts.remove(text)
            except Exception as e:
                print("Error: {}".format(str(e)))


    #utility
    def processSearch(self,t):
        t = t.replace('who is', '')
        t = t.replace('who was', '')
        t = t.replace('who are', '')
        t = t.replace('who were', '')
        t = t.replace('what is', '')
        t = t.replace(' an ',' ')
        t = t.replace(' a ',' ')
        t = t+'?'
        return(t)



if __name__ == '__main__':
    va = VA('Hello')
    va.Main()
