import speech_recognition as sr
import pyttsx3

class VA:
    def __init__(self):
        self.engine = pyttsx3.init()
        self.r = sr.Recognizer()
        self.loop = asyncio.get_event_loop()
        self.audios = []
        self.texts = []

    def Main(self):
        while True:

            self.listen()
            self.stt()
            self.tts()



    def listen(self):
        with sr.Microphone() as source:
          self.r.adjust_for_ambient_noise(source)  # here
          print("Say something!")
          self.audios.append(self.r.listen(source))

    def stt(self):
        for audio in self.audios:
            try:
                self.texts.append(self.r.recognize_sphinx(audio))
                self.audios.remove(audio)
            except LookupError:
                print("Could not understand audio")
            except Exception as e:
                print('Error: {}'.format(str(e)))

    def tts(self):
        for text in self.texts:
            try:
                self.engine.say(text)
                print(text)
                self.engine.runAndWait()
                self.texts.remove(text)
            except Exception as e:
                print("Error: {}".format(str(e)))



if __name__ == '__main__':
    va = VA()
    va.Main()
