#! /usr/bin/env python3

#0.1.2
#Key words only

import speech_recognition as sr
import pyttsx3

class VA:
    def __init__(self,kw):
        self.engine = pyttsx3.init()
        self.r = sr.Recognizer()

        self.audios = []
        self.texts = []
        self.kw = kw.lower().split(" ")
        with sr.Microphone() as source:
            self.r.adjust_for_ambient_noise(source)

    def Main(self):
        while True:

            self.listen()

            self.stt()
            # self.check_for_keyword()
            self.tts()




    def listen(self):
        try:
            with sr.Microphone() as source:

              print("Say something!")
              self.audios.append(self.r.listen(source, snowboy_configuration=('/root/test/va/snowboy/swig/Python3/', ['God.pmdl'])))
        except Exception as e:
            print('Error: {}'.format(str(e)))

    def stt(self):
        for audio in self.audios:
            try:
                self.texts.append(self.r.recognize_google(audio))
                self.audios.remove(audio)
            except LookupError:
                print("Could not understand audio")
            except Exception as e:
                print('Error: {}'.format(str(e)))

    # def check_for_keyword(self):
    #     kw = self.kw
    #     for i, text in enumerate(self.texts):
    #         try:
    #             ts = text.split(' ')
    #             if ts[:len(kw)] == kw:
    #                 ts = ts[len(kw):]
    #                 self.texts[i] = ' '.join(ts)
    #                 print(text)
    #                 print(1)
    #             else:
    #                 self.texts.remove(text)
    #                 print(0)
    #         except Exception as e:
    #             print('Error: {}'.format(str(e)))

    def tts(self):
        for text in self.texts:
            try:
                self.engine.say(text)
                print(text)
                self.engine.runAndWait()
                self.texts.remove(text)
            except Exception as e:
                print("Error: {}".format(str(e)))



if __name__ == '__main__':
    va = VA('Hello')
    va.Main()
