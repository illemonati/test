public class NativeHelloWorld{
  public native static void printHello();
  public static void main(String... args){
    System.loadLibrary("NativeHelloWorld");
    printHello();
  }
}
