const electron = require('electron');

const {app, BrowserWindow} = require('electron');

  let win;

  function createWindow(page){
    win = new BrowserWindow({width: 1400, height: 800});
    win.loadFile(page);
    // win.setMenu(null);
    win.webContents.session.webRequest.onHeadersReceived({}, (d, c) => {
    if(d.responseHeaders['x-frame-options'] || d.responseHeaders['X-Frame-Options']){
        delete d.responseHeaders['x-frame-options'];
        delete d.responseHeaders['X-Frame-Options'];
    }
    c({cancel: false, responseHeaders: d.responseHeaders});
  });

    win.on('closed', () => {
      win = null;
    })
  }

  function createMainWindow(){
    createWindow('index.html');
  }

app.on('window-all-closed',() => {
  if(process.platform !== 'darwin'){
    app.quit();
  }
})

app.on('activate', () => {
  if (win = null){
    createMainWindow();
  }
})


app.on('ready', createMainWindow);
