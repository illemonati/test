

struct Solution;

impl Solution {
    pub fn find(coin: i32, total:i32) -> Vec<(i32, i32)> {
        let mut single = 0;
        let mut amount_of_coins = 0;
        let mut result = vec![];
        while single < total {
            single = amount_of_coins * coin;
            result.push((amount_of_coins, single));
            amount_of_coins += 1;
        }
        result
    }

    pub fn coin_change(mut coins: Vec<i32>, total: i32) -> i32 {
        coins.sort();
        let mut coin_amounts = vec![];
        for coin in coins {
            coin_amounts.push(Solution::find(coin, total));
        }

        let mut all_possible_combinations = vec![];
        for combinations in &coin_amounts {
            for (amount_of_coins, coin_amount) in coin_amounts[0].iter() {
                all_possible_combinations.push(vec![(*amount_of_coins, *coin_amount)])
            }
        }
        for combinations in coin_amounts[1..].iter() {
            for (index, (amount_of_coins, coin_amount)) in combinations.iter().enumerate() {
                for j in 0..coin_amounts.len() {
                    all_possible_combinations[index + j * coin_amounts[0].len()].push((*amount_of_coins, *coin_amount));
                }
            }
        }
        println!("{:?}", all_possible_combinations);
        let mut results = vec![];
        for combinations in all_possible_combinations {
            let mut coins_used = 0;
            let mut sum = 0;
            for combination in combinations {
                coins_used += combination.0;
                sum += combination.1;
            }
            results.push((coins_used, sum));
        }
        println!("{:?}", results);
        let mut min_coins = -1;
        for combination in results {
            if combination.1 == total {
                if (combination.0 < min_coins) || (min_coins == -1) {
                    min_coins = combination.0;
                }
            }
        }
        return min_coins;
    }
}

fn main() {
    let k = Solution::coin_change(vec![1, 2, 3], 11);
    println!("{}", k);
}
