#![crate_type="lib"]

struct Solution;

impl Solution {
    pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
        for (id, i) in nums.iter().enumerate() {
            for (jd, j) in nums.iter().enumerate() {
                if (i + j == target) && (id != jd) {
                    return vec![id as i32, jd as i32];
                }
            }
        }
        unreachable!();
    }
}
