#![crate_type="lib"]
#![feature(int_error_matching)]
use std::i32;


    struct Solution;

    impl Solution {
        pub fn my_atoi(s: String) -> i32 {
            let mut answer = String::new();
            for c in s.chars() {
                if c == '-' {
                    answer.push(c);
                } else if let Some(_) = c.to_digit(10) {
                    answer.push(c);
                } else if c == ' ' {
                    continue;
                } else {
                    break;
                }
            }
            println!("{}", &answer);
            match answer.parse::<i32>() {
                Ok(n) => n,
                Err(e) => match e.kind() {
                    Underflow => i32::MIN,
                    Overflow => i32::MAX,
                    _ => 0,
                }
            }
        }
    }
