from warframe_alerts_rss import WarframeAlertsRss, WarframeAlert

wa = WarframeAlertsRss()
alerts = wa.get_alerts()

for alert in alerts:
    print("title: {}\n type: {}\n\n\n".format(alert.title, alert.type))
