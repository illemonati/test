

import java.util.Arrays;
import java.util.Random;

public class Randp {
    private int[] nums;
    private int numsLeft;

    public Randp(int n) {
        this.nums = new int[n];
        initNums(n);
    }

    private void initNums(int n) {
        for(int i = 1; n+1 > i; i++) {
            this.nums[(i - 1)] = i;
        }
        numsLeft = n;
    }

    public int nextInt() {
        //get result
        int result = 0;
//        for (int q : this.nums) {
//            System.out.print(q);
//        } System.out.println();
        if (numsLeft > 0) {
            int min = 1;
            int max = this.numsLeft;
            if (max < 1) {
                max = 1;
            }

            int index = 0;
            if (this.nums.length > 0) {
                Random rand = new Random();
                //            index = min + (int) ((Math.random()) * ((max - min)));
                index = (rand.nextInt((max - min) + 1) + min);
                result = this.nums[index - 1];
            }

            //change array

            if (this.numsLeft != index) {
                //            if (index == -1) {
                //                index = 0;
                //            }
                this.nums[index - 1] = this.nums[this.numsLeft - 1];
            }
            //        this.nums = Arrays.copyOf(this.nums, this.nums.length - 1);

            this.numsLeft--;
            //        System.out.print(index);
        }

        return result;
    }
}