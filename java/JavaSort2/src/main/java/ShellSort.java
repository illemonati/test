// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ShellSort.java

import java.awt.Dimension;

public class ShellSort extends SortingPanel
{

    public ShellSort(ToolBar toolBar, Dimension parentPanelSize)
    {
        super(toolBar, parentPanelSize);
    }

    public void runSort()
    {
        int h = 1;
        do
            h = 3 * h + 1;
        while(h < super.sortNumElem);
        do
        {
            h /= 3;
            for(int i = h; i < super.sortNumElem; i++)
            {
                int v = i;
                for(int j = i; j >= h && isLessThan(j, j - h); j -= h)
                {
                    if(super.killProcess)
                        return;
                    swap(j, j - h);
                }

            }

        } while(h > 1);
        setSortDone();
    }
}
