// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   SelectionSort.java

import java.awt.Dimension;

public class SelectionSort extends SortingPanel
{

    public SelectionSort(ToolBar toolBar, Dimension parentPanelSize)
    {
        super(toolBar, parentPanelSize);
    }

    public void runSort()
    {
        int least = 0;
        for(int i = 0; i < super.sortNumElem - 1; i++)
        {
            least = i;
            for(int j = i; j < super.sortNumElem; j++)
            {
                if(super.killProcess)
                    return;
                if(!isLessThan(least, j))
                    least = j;
            }

            if(i != least)
                swap(least, i);
            setIsSorted(i);
        }

        setSortDone();
    }
}
