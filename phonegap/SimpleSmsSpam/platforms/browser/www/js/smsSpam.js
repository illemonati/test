// sms options
var sms_options = {
           replaceLineBreaks: true, // true to replace \n by a new line, false by default
           android: {
                intent: 'INTENT'  // send SMS with the native android SMS messaging
              // intent: '' // send SMS without open any other app
           }
       };




window.onload=function(){
  setStatus("ready");
  document.getElementById("runButton").addEventListener("click", prepSmS);

  function setStatus(status){
    document.getElementById("status").innerHTML = "<p>" + status + "</p>";
  }

  function prepSmS(e){
    var r1 = false, r2=false, r3=false;
    if (document.getElementById("phoneNum").value.length == 0){
      status = "Please enter a valid phone number !!!";
    }else{
      var phoneNum = Math.floor(document.getElementById("phoneNum").value).toString();
      r1 = true;
    }
    if ((document.getElementById("spamAmount").value.length == 0) || (Math.floor(document.getElementById("spamAmount").value) == 0 )){
      status = "Please enter a valid amount of spam (more than 0)!!!";
    }else{
      var spamAmount = Math.round(document.getElementById("spamAmount").value);
      r2 = true;
    }
    if (document.getElementById("spamMessage").value.length == 0){
      status = "Please enter a message longer than the length of 0 !!!";
    }else{
      var spamMessage = document.getElementById("spamMessage").value.toString();
      r3=true;
    }
    if(r1 && r2 && r3){
      status = "Prep Done";
      setStatus(status);
      sendSmS(phoneNum, spamAmount, spamMessage);
    }
    setStatus(status);
  }

  function sendSmS(phoneNum, spamAmount, spamMessage){
    // for(var i = 0; i < spamAmount; i++){
    //   sms.send(phoneNum, spamMessage, sms_options);
    //   setStatus( (i + 1) + "/" + spamAmount);
    // }
    var success = function () { alert('Message sent successfully'); };
    var error = function (e) { alert('Message Failed:' + e); };
    sms.send(phoneNum, spamMessage, sms_options, success, error);

    // alert(1);
  }

};
