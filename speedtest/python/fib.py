def fib(n: int) -> int:
    a = 0
    b = 1
    for _ in range(1, n):
        a, b = b, a+b
    return b


if __name__ == "__main__":
    print(fib(75))
