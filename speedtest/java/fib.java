class Fib{
    public static long fib(long n) {
        long a = 0;
        long b = 1;
        for (long i = 1; i < n; ++i) {
            long temp = a;
            a = b;
            b += temp;
        }
        return b;
    }
   public static void main(String[] args) {
       System.out.println(fib(75));
   }
}


