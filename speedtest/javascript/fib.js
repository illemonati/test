var fib = function (n) {
    var a = 0;
    var b = 1;
    for (var i = 1; i < n; ++i) {
        var temp = a;
        a = b;
        b += temp;
    }
    return b;
};
console.log(fib(75));
