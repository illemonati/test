
const fib = (n: number) => {
    let a = 0;
    let b = 1;
    for (let i = 1; i < n; ++i) {
        let temp = a;
        a = b;
        b += temp;
    }
    return b;
}

console.log(fib(75))



