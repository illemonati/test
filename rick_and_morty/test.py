import asyncio, aiohttp


async def getAll():
    async with aiohttp.ClientSession() as session:
        async with session.get("https://rickandmortyapi.com/api/character/?page=2") as response:
            print(response.status)
            print(await response.text())

loop = asyncio.get_event_loop()
loop.run_until_complete(getAll())
loop.close()
