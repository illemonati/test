import asyncio, aiohttp
import re

async def getPosts():
    async with aiohttp.ClientSession() as sess:
        async with sess.get("https://tw.tong.icu") as resp:
            resp_t = await resp.text()
            titles = re.findall('(?s)(?<=<h3).*?(?=</h3>)', resp_t)
            titles = [re.findall('\>(.*)', title)[0] for title in titles]
            # for title in titles:
            #     title = re.findall('\>(.*)', title)[0]
            print(titles)

            

loop = asyncio.get_event_loop()
loop.run_until_complete(getPosts())
loop.close()
