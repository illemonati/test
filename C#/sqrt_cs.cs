using System;

public class sqrt_cs{

  public static double sqrt(double d){
    return d*d;
  }

  public static int Main(string[] args){
    try{
      double og = Convert.ToDouble(args[0]);
      double answer = sqrt(og);
      Console.WriteLine(answer);
    }catch{
      Console.WriteLine("Something went wrong, try putting in a argument.");
    }

    return 0;
  }


}
