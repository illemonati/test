using System;
using System.Collections.Generic;


public class v_t{

  public static double sqrt(double i){
    return i*i;
  }


  public static int Main(string[] args){

    var productName = "TV";
    int productYear = 1234;
    float productPrice = 1.2F;


    int[] nums = {1,2,3,4,5};

    int[,] matrix = new int[2,1] { {1},{1} };
    // string productName = "TV";
    List<string> listO_String = new List<string>();
    List<string> lOS = new List<string> {"ooga", "good"};
    Dictionary<string, int> dOS= new Dictionary<string,int>();


    lOS.Add("hi");
    lOS.Remove("ooga");
    lOS.Add("NO");
    lOS.RemoveAt(lOS.Count-1);

    Console.WriteLine("hi");
    Console.WriteLine(productName);
    Console.WriteLine(productYear);
    Console.WriteLine(productPrice);

    dOS.Add("Hello", 1);
    dOS["Hi"] = 2;

    for(int i = 0; i< 5; i++){
      Console.Write(nums[i]);
    }
    Console.WriteLine();

    foreach(string ele in lOS){
      Console.Write(ele + " ");
    }
    Console.WriteLine();

    foreach(int num in nums){
      Console.Write(sqrt(num)+" ");
    }
    Console.Write("\n");

    return 0;

  }
}
